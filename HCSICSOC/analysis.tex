% !TEX root = paper.tex
\section{Automated Analysis}
\label{sec:analysis}

In this Section we propose an operational semantics \cite{Hofstede98} of \hcss which primary goal is providing these models with an automated support for certain analysis operations. These operations will enable to check the satisfaction of the proposed validity criteria and support the search of the most suitable configuration among other capabilities.

\glsreset{sfm}
\glsreset{fm}

The target domain of the proposed semantics are \sfms \cite{trinidad2013automated,trinidad14-CFM}. \sfms are intended to describe all the possible configurations of variability-intensive systems and assist their configuration. An \sfm is divided in two parts: the \fm, and a \cm. The \fm defines the variable parts of a system in terms of features which are hierarchically organised by means of different kinds of relationships, forming a tree-like structure. Attributes can be linked to features to indicate any further information that could be relevant to configure the system. The \cm enables a user to specify their needs, selecting or removing features and adding constraints for attribute values. The main strong point of mapping \hcss to \sfms is taking advantage of the \aasfm, which provides a wide catalogue of analysis operations in terms of which the demanded \hcs analysis operations can be solved.

\begin{table}[t]
\begin{center}
	\caption{Mapping CSs into SFMs}
	\label{tab:CSmapping}
\begin{tabular}{ c  c }
\midrule
\textbf{HCS} & \textbf{SFM} \\ 
\midrule
\begin{minipage}[c]{0.35\textwidth}
\textbf{Service} CSName \{ ... \}
\end{minipage}
&
\begin{minipage}[c]{0.35\textwidth}
\includegraphics[scale=0.55]{figures/CSRootMapping.pdf}
\end{minipage}  
 \\
\midrule
\begin{minipage}[c]{0.35\textwidth}
\textbf{\%SelectableTerms}\\ 
$T_k$: \{$v_{k,1},...,v_{k,j}$\};\\
\end{minipage}
 & 
\begin{minipage}[c]{0.35\textwidth}
\includegraphics[scale=0.55]{figures/CSSelectableTermsMapping.pdf}
\end{minipage}  
 \\
 \midrule
\begin{minipage}[c]{0.35\textwidth}
\textbf{\%DerivedTerms}\\ 
$T_1$: \{$v_{1,1},...,v_{1,j}$\};\\
$T_2$: int[$v_{2,min},v_{2,max}$] ;\\
$T_3$: real[$v_{3,min},v_{3,max}$]  ;\\
...\\
$T_n$: boolean;\\
\end{minipage}
&
\hspace{-1cm}
\begin{minipage}[c]{0.35\textwidth}
\includegraphics[scale=0.55]{figures/CSDerivedTermsMapping.pdf}
\end{minipage}  
 \\
\midrule
\begin{minipage}[c]{0.35\textwidth}
\textbf{\%Dependencies}\\ 
$C_1$;
...
$C_n$;\\
\end{minipage}
&  \makebox[0.5cm]{}Constraints on the $SFM$ features/attributes\\
\midrule
\end{tabular}
\end{center}
\end{table}

We present three mapping tables that summarise the procedure to generate an \sfm from an \hcs. First, Table \ref{tab:CSmapping} shows a mapping from a single configurable service to an \sfm. We create a root feature for every service, and a child (mandatory) feature for each selectable term. Besides, each term value is mapped into a feature. These features are grouped to their corresponding selectable term feature by means of an alternative relationship. This structure obligues a user to select one value for each selectable term in order to define a configuration for the service. The derived terms are mapped as global attributes linked to the root feature with their corresponding types. Additionally, $p$ attributes are added to allow a user to indicate their preference for a given selectable or derivable term.

\begin{table}[t]
\begin{center}
	\caption{Mapping items, requirements and preferences into SFMs}
	\label{tab:NeedMapping}
\begin{tabular}{ l  c }
\midrule
\multicolumn{1}{c}{\textbf{HCS}}  & \textbf{SFM} \\ 
\midrule
\multicolumn{2}{c}{\emph{Items}} \\
\midrule
\begin{minipage}[c]{0.3\textwidth}
\#\#On user HCS def. \\
\textbf{\%Services}\\ 
CSName[n,m] CSAlias;\\
\\
\#\#On user needs def. \\
\textbf{\%Items}\\ 
CSAlias[$Item_1$,...,$Item_k$];\\
\end{minipage}
&
\begin{minipage}[c]{0.35\textwidth}
\includegraphics[scale=0.55]{figures/HCSServiceInstance.pdf}
\end{minipage}  
\\
\midrule
\multicolumn{2}{c}{\emph{Requirements}} \\
\midrule
\begin{minipage}[c]{0.3\textwidth}
\textbf{\%Requirements}\\ 
$R_1$;
...
$R_n$;\\
\end{minipage}
&  \makebox[0.5cm]{}Constraints on the $SFM$ features/attributes\\
\midrule
\multicolumn{2}{c}{\emph{Preferences}} \\
\midrule
\textbf{Favorites}($T_k$,$V_{k,j}$) & $V_{k,j}=sel \Leftrightarrow T_k.p = 1 \wedge V_{k,j}=rem \Leftrightarrow T_k.p = 0$\\
\textbf{Dislikes}($T_k$,$V_{k,j}$) & $V_{k,j}=rem \Leftrightarrow T_k.p = 1 \wedge V_{k,j}=sel \Leftrightarrow T_k.p = 0$\\
\textbf{Highest}($T_k$) & $HCSName.p_{T_k} = \frac{V_{k} - V_{k,min}}{V_{k,max} -V_{k,min}}$\\
\textbf{Lowest}($T_k$) & $HCSName.p_{T_k} = \frac{V_{k,max} - V_{k}}{V_{k,max} -V_{k,min}}$\\
\textbf{Around}($T_k$,$V_{k,j}$) & $HCSName.p_{T_k} = \frac{max(V_{k,j} - V_{k,min}, V_{k,max} - V_{k,j}) - |V_{k} - V_{k,j}|}{max(V_{k,j} - V_{k,min}, V_{k,max} - V_{k,j})}$\\
\midrule
\multicolumn{2}{c}{\emph{Preferences Composition}} \\
\midrule
& $HCSName.p = \sum_{k} T_k.p + \sum_{i} HCSName.p_{T_i}$\\
\midrule
\end{tabular}
\end{center}
\end{table}

Second, the \hcs and the user needs must be mapped together, since the specific number of instances to be created for each configurable service within the \hcs must be known to generate the adequate \sfm. Table \ref{tab:HCSmapping} and \ref{tab:NeedMapping} show how \hcs elements and user needs are mapped into an \sfm. For each item created in user needs, an \sfm is created following the mapping proposed for configurable services. Then, an \hcs root feature is created as a way to bind all the item-specific root features by means of mandatory relationships. Then, attributes are created for \hcs-specific terms following the same process than for configurable services. Finally, user needs, either requirements or preferences are mapped into \sfm constraints as shown in Table \ref{tab:NeedMapping}.

\begin{table}[t]
\begin{center}
	\caption{Mapping HCSs into SFMs}
	\label{tab:HCSmapping}
\begin{tabular}{ c  c }
\midrule
\textbf{HCS} & \textbf{SFM} \\ 
\midrule
\begin{minipage}[c]{0.40\textwidth}
\textbf{Highly-configurable Service} \\
HCSName \{ ... \}
\end{minipage}
&
\begin{minipage}[c]{0.35\textwidth}
\includegraphics[scale=0.55]{figures/HCSRootMapping.pdf}
\end{minipage}  
 \\
 \midrule
\begin{minipage}[c]{0.40\textwidth}
\textbf{\%Terms}\\ 
$T_1$: \{$v_{1,1},...,v_{1,j}$\};\\
$T_2$: int[$v_{2,min},v_{2,max}$] ;\\
$T_3$: real[$v_{3,min},v_{3,max}$]  ;\\
...\\
$T_n$: boolean;\\
\end{minipage}
&
\hspace{-1cm}
\begin{minipage}[c]{0.35\textwidth}
\includegraphics[scale=0.55]{figures/HCSTermsMapping.pdf}
\end{minipage}  
 \\
\midrule
\begin{minipage}[c]{0.40\textwidth}
\textbf{\%Dependencies}\\ 
$C_1$;
...
$C_n$;\\
\end{minipage}
&  \makebox[0.5cm]{}Constraints on the $SFM$ features/attributes\\
\midrule
\end{tabular}
\end{center}
\end{table}

Figure~\ref{fig:StorageSFM} showcases the result of applying the presented mappings to the whole example scenario that we used throughout the paper, described in Listings~\ref{code:StorageSynopsis},~\ref{code:StorageHCSSynopsis}, and~\ref{code:simpleConfiguration}.

\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{figures/StorageSFM.pdf}
\caption{Volume Storage HCS translated into a SFM.}
\label{fig:StorageSFM}
\end{figure}

The resulting \sfm can be analysed using existing \aasfm tools. So for example, a false configurable service can be detected if the resulting \sfm is void. Table \ref{tab:operationCorrespondence} shows a correspondence table with the \aasfm operations that can be used to perform \hcs analysis operations.

\begin{table}[tb]
\centering
\caption{Analysis operation correspondence between \hcss and \sfms}
\label{tab:operationCorrespondence}
\begin{tabular}{ll}
\textbf{\hcs operation}&\textbf{\sfm operation}\\
\hline
Inconsistent service&Void \sfm\\
Dead value&Dead feature\\
False decision term&False-optional feature\\
Redundant dependency&Product listing (composite)\\
Best configuration&Best product\\
\hline
\end{tabular}
\end{table}

