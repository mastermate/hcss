% !TEX root = paper.tex
\section{Validity Criteria}
\label{sec:validity}

A configurable service may present different anomalies regarding its configuration capabilities. For example, it is possible that some values of a configurable term are no selectable under any circumstance, or that a configurable term is not such configurable. In order to illustrate the anomalies, we define the validity criteria for configurable services using the Volume Storage Service of Listing~\ref{code:StorageSynopsis} as a running example. The validity criteria are categorised in three levels:
\begin{inparaenum}[(1)]
\item the \textbf{warning level}, which encompasses anomalies that do not damage the configuration capabilities of the service; 
\item the \textbf{term error level}, which encompasses anomalies that damage the configurations capabilities of the service, and in particular of given values and terms; and
\item the \textbf{service error level}, where the errors of this level make the service not configurable or directly inconsistent.
\end{inparaenum}

\subsection{Warning Level}

At the first validity level, i.e. the warning level, the anomalies detected do not affect the configuration capabilities of the service, but may harden the understanding of the decision space. In particular, we have identified one possible anomaly at the warning level: the redundant dependency.

A \emph{redundant dependency} has no effect on the decision space of the service. If such dependency is removed, the resultant decision space remains unaltered. For instance, the dependency \texttt{SSD == false -> costGBMonth < 0.1;} would be redundant for the storage service of Listing~\ref{code:StorageSynopsis}. The dependency says ``if the disk is not SSD-based, the GB cost/month should be lower than 0.1 \$'', while at the same time we say in the pricing table of Listing~\ref{code:StorageSynopsis} that the prices for SSD-based volumes are 0.05, 0.06 and 0.08 per GB. In this way, such dependency does not modify the decision space, and can be classified as redundant.


\subsection{Term Error Level}

We name this second validity level as the term error level. Although at this level the service still presents multiple configurations, these errors damage its configuration capabilities. We identify two types of errors that affect single values and terms: dead values and false decision terms. 

A \emph{dead value} in a selectable term is a value which cannot be selected under any circumstances, i.e. there is no configuration in the decision space where that value can be chosen. In this way, although the value can be apparently chosen, existing dependencies make it non selectable. In Listing~\ref{code:DeadAndFalseSynopsis} we show an example of dead values for the storage service of Listing~\ref{code:StorageSynopsis}. 
We find a constraint that denies the selection of USA as a region, making such value dead.
At the same time, JP region is also dead, since only SSD-based instances can be selected in such location, but the cost/GB should be at most 0.12 \$, while in the case of SSD is 0.15 \$.

\begin{lstlisting}[style=synopsis,caption={Example of Dead Value and False Decision Term},label={code:DeadAndFalseSynopsis}]
	Region == "JP" -> SSD == true;
	Region == "JP" -> costGBMonth <= 0.12;
	Region != "USA";
\end{lstlisting}

If all the term values but one of a given decision term are dead, we say the term is a \emph{false decision term}. Although the term can apparently be configurable, there is no possible decision: the consumer is forced to select the same particular value in every configuration in the decision space. Consequently, a false decision term makes all the remaining alternatives for its configuration option to be dead.

The example in Listing~\ref{code:DeadAndFalseSynopsis} also generates a false decision term. Given that the term \texttt{Region} only has three values, the death of two of them makes the term a false decision term. In this case, the consumer cannot choose among three regions, but has to select \texttt{``EU''} always.

\subsection{Service Error Level}

Finally, at the third validity level, namely service error level, the service presents one or none configurations, so consequently these errors are the most critical ones. We identify two types of service errors: false configurable service and inconsistent service.

A service is a \emph{false configurable service} when there is only a single available configuration. In other words, all the decision terms of a false configurable service are false decision terms, i.e. there are no real choices since the decision space contains only one possible configuration. In Listing~\ref{code:FalseConfigServiceSynopsis} we show a set of dependencies that make the Volume Storage Service a false configurable service. In this way, only one configuration can be selected: \texttt{Region == ``EU'}', \texttt{SSD == true}.

\begin{lstlisting}[caption={Example of False Configurable Service.},label={code:FalseConfigServiceSynopsis},
style=synopsis]
	Region == "JP" -> SSD == true;
	Region == "JP" -> costGBMonth <= 0.12;
	Region != "USA";
	SSD == false -> Region == "USA";
\end{lstlisting}

When all the values of a decision term are dead, we say that the service is an \emph{inconsistent service}. This means that there is no available configuration for the service (i.e. its decision space is empty), and consequently it cannot be delivered to the consumer. For instance, if we add to the Volume Storage Service the additional dependency \texttt{costGBMonth >= 0.2}, the service becomes inconsistent due to the conflict with the pricing defined in the table.


